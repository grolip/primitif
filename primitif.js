const dgram = require('dgram');
const crypto = require('crypto');
const EventEmitter = require('events');
const redis = require('redis');
const config = require('./config.json');

class Api extends EventEmitter {}
const api = new Api();
const algorithm = 'aes-256-ctr';
const ivSize = 16; // Caractères.
const expireReqDemand = 12; // Secondes.
const eventReqDemand = 'request.demand';
const errors = [
	'BadRequest', 
	'Unauthorized', 
	'NotFound',
	'TooManyRequest',
	'InternalServerError',
];

/*
*
* Primitif v1
* Deprecated since #1 version !
* Anton Millescamps
* 
*/

/* Gestion des variables du fichier de configuration. */
config.min_request_size = 40; //Caractères.

if (!config.hasOwnProperty('debug'))
	config.debug = false;

if (!config.hasOwnProperty('matricule_size'))
	config.matricule_size = 8; //Caractères.

if (!config.hasOwnProperty('max_request_size'))
	config.max_request_size = 512; //Caractères.

if (!config.hasOwnProperty('ip_version'))
	config.ip_version = 4;

if (!config.hasOwnProperty('redis_address'))
	config.redis_address = '127.0.0.1';

if (!config.hasOwnProperty('redis_port'))
	config.redis_port = 6379;


/* FONCTIONS D'EXCEPTIONS PERSONNALISEES */

function BadRequest (){
	this.message = 'Bad Request';
	this.name = 'BadRequest';
	this.code = 400;
}


function Unauthorized (){
	this.message = 'Unauthorized';
	this.name = 'Unauthorized';
	this.code = 403;
}


function NotFound (){
	this.message = 'Not Found';
	this.name = 'NotFound';
	this.code = 404;
}


function TooManyRequest (){
	this.message = 'Too Many Request';
	this.name = 'TooManyRequest';
	this.code = 429;
}


function InternalServerError (){
	this.message = 'Internal Server Error';
	this.name = 'InternalServerError';
	this.code = 500;
}


/* FONCTION DE GESTION DES REQUEST DEMAND */

function get_key (matricule, endpoint){
	/* Retourne la clé générée à partir du prefix, du matricule et du
	endpoint */
	var key = matricule + '_' + endpoint;
	return key;
}


function rand_request_id (){
	// Génération aléatoire d'une request to request.
	return crypto.randomBytes(8).toString('hex');
}


function add_request_demand (key, matricule, endpoint){
	// Ajout d'une request to request dans le cache Redis.
	var reqid = rand_request_id();
	redcli.hmset(key, {
		endpoint: endpoint, 
		request_id: reqid,
		matricule: matricule,
	});
	redcli.expire(key, expireReqDemand);
	return reqid;
}


/* FONCTIONS DE CHIFFREMENT */

function encrypt (password, iv, data){
	var cipher = crypto.createCipheriv(algorithm, password, iv);
	var crypted = cipher.update(data, 'utf8', 'hex');
	crypted += cipher.final('hex');
	return crypted;
}


function decrypt (password, iv, data){
	var decipher = crypto.createDecipheriv(algorithm, password, iv);
	var dec = decipher.update(data, 'hex', 'utf8');
	dec += decipher.final('utf8');
	return dec;
}


/* FONCTIONS DE RENDU */

function render_response (client, data, args){
	if (args != null){
		data.endpoint = args.endpoint;
		data.request_id = args.request_id;
	}
	data = JSON.stringify(data);
	data = config.matricule + encrypt(client.password, client.iv, data);
	server.send(data, client.port, client.address);
}


function render_error (client, e){
	var data = {ok: false};
	// On s'assure que l'erreur doit être notifié au client.
	if (client != null){
		if (errors.indexOf(e.name) != -1){
			data.message = e.name;
			data.code = e.code;
		}
		else {
			// Erreur Générique.
			data.message = 'UnknownError';
			data.code = 520;
		}
		render_response(client, data);
	}
	// Retour console en mode debug.
	if (config.debug === true){
		console.log(e.name + '>' + e.message);
	}
}


/* FONCTIONS DE PARSING */

function parse_matricule (msg){
	// Récupération du matricule.
	var matSize = config.matricule_size;
	var matricule = msg.toString('utf8').substring(0, matSize);
	if (matricule.length === matSize){
		let rule = new RegExp('[a-zA-Z0-9]{' + matSize + '}');
		if (!rule.test(matricule)){
			matricule = null;
		}
	}
	else {
		matricule = null;
	}
	return matricule;
}


function parse_data (msg, client){
	// Récupération des données.
	try {
		var matSize = config.matricule_size;
		var data = msg.toString('utf8').substring(matSize + ivSize);
		data = decrypt(client.password, client.iv, data);
		data = JSON.parse(data);
	} catch (e){
		data = null;
	}
	return data;
}


function parse_iv (msg){
	/* Récupération de l'iv dans la requete */
	var matSize = config.matricule_size;
	var posIv = matSize + ivSize;
	return msg.toString('utf8').substring(matSize, posIv);
}


/* FONCTIONS DE VERIFICATION */

function verify_request_demand (req, data, client){
	// Vérifier la concordance de la demande.
	var verified = false;
	if (req != null &&
		req.request_id === data.request_id && 
		req.endpoint === data.endpoint &&
		req.matricule === client.matricule){
			verified = true;
	}
	return verified;
}


function verify_request_size (size){
	// Vérifier la taille de la requete.
	var verified = false;
	var min = config.min_request_size;
	var max = config.max_request_size;
	if (size >= min && size <= max){
		verified = true;
	}
	return verified;
}


function verify_client_auth (client){
	/* Vérification de l'existence du client et de son droit à se 
	connecter */
	var verified = false;
	// Vérification des erreurs au niveau de Redis.
	if (client != null){
		if (client.hasOwnProperty('allowed_ip') && 
		client.allowed_ip.length > 0){
			// Parsing liste ip autorisées.
			var allowed_ip = client.allowed_ip.split(',');
			if (allowed_ip.indexOf(client.address) != -1){
				verified = true;
			}
		}
		else {
			verified = true;
		}
	}
	return verified;
}


function verify_endpoint_exist (endpoint){
	// Vérifier que la methode existe.
	var verified = false;
	var listApi = api.eventNames();
	if (listApi.indexOf(endpoint) != -1 && endpoint != eventReqDemand){
		verified = true;
	}
	return verified;
}


function verify_endpoint_allowed (client, endpoint){
	// Vérifier que le client est autorisé à acceder à la methode.
	var verified = false;
	if (client.hasOwnProperty('allowed_method') && 
	client.allowed_method.length > 0){
		let allowedMethod = client.allowed_method.split(',');
		if (allowedMethod.indexOf(endpoint) != -1){
			verified = true;
		}
	}
	else {
		verified = true;
	}
	return verified
}


/* FONCTIONS DE GESTION DES EVENTS */

function execute_event (client, endpoint, data){
	// Vérification de la présence du request id.
	if (data.hasOwnProperty('request_id')){
		var key = get_key(client.matricule, endpoint)
		// Récupération de la demande de requête.
		redcli.hgetall(key, (err, req) => {
			try {
				if (err === null){
					// Vérification d'un demande préalable.
					if (verify_request_demand(req, data, client)){
						redcli.del(key);
						api.emit(endpoint, client, data);
					}
					else
						throw new Unauthorized();
				}
				else
					throw new InternalServerError();
			} catch (e){
				render_error(client, e);
			}
		});
		// Fin callback hgetall.
	}
	else {
		// Création d'une demande de requete.
		api.emit(eventReqDemand, client, endpoint);
	}
}


function routing_event (client, data){
	// Vérifier que le paramètre endpoint est inclu dans l'objet data.
	if (data.hasOwnProperty('endpoint')){
		if (verify_endpoint_exist(data.endpoint)){
			// Vérifier droit d'accès à la méthode.
			if (verify_endpoint_allowed(client, data.endpoint))
				execute_event(client, data.endpoint, data);
			else
				throw new Unauthorized();
		} 
		else
			throw new NotFound();
	} 
	else
		throw new BadRequest();
};


/* Déclaration du serveur et des clients Redis */
const redcli = redis.createClient(config.redis_port,
	config.redis_address);
const server = dgram.createSocket('udp' + config.ip_version);


server.on('message', (msg, rinfo) => {
	// Vérification de la taille minimale de la requete.
	if (verify_request_size(rinfo.size) === true){
		// Récupération du matricule.
		var matricule = parse_matricule(msg);
		if (matricule != null){
			// Récupération de l'iv en vue du dechiffrement.
			var iv = parse_iv(msg);
			// Récupération des infos du clients dans cache Redis.
			redcli.hgetall(matricule, (err, client) => {
				// Vérification lecture redis
				if (err === null){
					try {
						// Vérification autorisation.
						if (verify_client_auth(client)){
							/* Ajout de données complémentaires 
							au client. */
							client.matricule = matricule;
							client.address = rinfo.address;
							client.port = rinfo.port;
							client.iv = iv;
							// Récupération des données.
							var data = parse_data(msg, client);
							if (data != null){
								routing_event(client, data);
							}
						}
					} catch (e){
						render_error(client, e);
					}
				}
			});
			// Fin callback hgetall.
		}
		// Fin condition matricule.
	}
});


server.on('listening', () => {
	console.log('Primitif listening ...');
});


// Fonction d'api de demande de requete.
api.on(eventReqDemand, (client, endpoint) => {
	// Méthode de création de demande de requete.
	var key = get_key(client.matricule, endpoint);
	redcli.hgetall(key, (err, req) => {
		try {
			if (err === null){
				if (req === null){
					let data = {ok: true};
					/* Enregistrement de la demande de requete et 
					recup du request_id. */
					data.request_id = add_request_demand(key, 
						client.matricule, endpoint);
					data.endpoint = eventReqDemand;
					data.target = endpoint;
					// Réponse au client.
					render_response(client, data);
				}
				else
					throw new TooManyRequest();
			}
			else
				throw new InternalServerError();
		} catch (e){
			render_error(client, e);
		}
	});
});


server.bind(config.port);

// Export des élements nécessaires.
module.exports.api = api;
module.exports.config = config;
module.exports.render_response = render_response;
module.exports.render_error = render_error;
module.exports.InternalServerError = InternalServerError;

