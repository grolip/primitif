# Primitif

Système basique d'API utilisant UDP.


## Introduction

Chaque requête se compose du matricule du client, le l'iv ainsi que
du corps de la requête en JSON chiffré avec AES.


## Création d'un utilisateur.

Les informations utilisateurs sont stockées dans un cache Redis. 
Elles sont composées au minimum de:

* matricule (str): qui fait office de clé dans le cache Redis. Se compose uniquement
de lettre minuscule et majuscule et de chiffre.
* password (str): se compose de 32 caractères.

Il est possible d'ajouter deux champs optionnels:

* allowed_ip (str): whitelist d'ip autorisé pour ce client.
* allowed_method (str): whitelist de méthode accessible pour ce client.

Note: pour les deux derniers champs, le caractère de séparation est la virgule.

Exemple:
```bash
$> redis-cli
hmset 10000001 "password" "mot de passe de 32 caractères .."
```

Le nouveau client à pour matricule 10000001.

Ajout d'objet dans Redis: https://redis.io/commands/hmset


## Configuration du serveur

Le fichier de configuration est au format JSON. Les informations obligatoires sont:

* ip (str): ip du serveur.
* port (int): port du serveur.
* matricule (str): matricule du serveur.

Les informations optionnelles sont:

* debug (bool): activer les options de déboguage (par défaut false).
* matricule_size (int): définir la taille du matricule dans les requêtes (par défaut 8).
* max_request_size (int): taille maximale des requêtes (par défaut 512 octets).
* ip_version (int): version du protocole IP (par defaut 4).
* redis_address (str): adresse du cache redis (par defaut 127.0.0.1).
* redis_port (int): port du cache redis (par defaut 6379).


Le fichier doit se trouver au même niveau que primitif.js et se nommer 'config.json'.

## Exemple d'API

Un exemple de base reprenant le principe du 'ping'.

```javascript
const primitif = require('./primitif.js');

const api = primitif.api;
const render_response = primitif.render_response;


api.on('echo.request', function (client, args){
	var data = {ok: true, msg: 'Echo reply'};
	render_response(client, data, args);
});
```

### Détails sur le traitement d'une requête

Reprenons l'exemple d'API ci-dessus avec son unique méthode: 'echo.request'.

* Le client envoi une demande de requête pour l'endpoint 'echo.request'.
* Le serveur lui renvoie un request_id unique en guise de jeton d'autorisation, 
uniquement valable pour l'endpoint 'echo.request'. Ce jeton est valable 12 secondes.
* Le client envoi une requête pour l'endpoint 'echo.request' contenant le request_id.
* Le serveur execute la méthode et lui renvoi le résultat.

Ce processus protège d'attaques par rejeu en rendant chaque requête unique.
Toutes les requêtes en dehors du matricule et de l'iv sont chiffrés 
par l'algo AES 256 mode CTR.

