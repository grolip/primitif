import socket
import json
import binascii
from Crypto.Cipher import AES
from Crypto import Random
import Crypto.Util.Counter


def new_iv ():
	return Random.new().read(32).hex()[:16]


def new_ctr (currentIv):
	iv = int(binascii.hexlify(currentIv.encode()), 16)
	return Crypto.Util.Counter.new(128, initial_value=iv)


class Primitif ():
	def __init__ (self):
		self.matricule = '00000001'
		self.password = 'testtesttest0000testtesttest0000'
		self.server_ip = '127.0.0.1'
		self.server_port = 9999
		self.iv = None


	def encrypt (self, data):
		data = json.dumps(data, separators=(',', ':'))
		ctr = new_ctr(self.iv)
		obj = AES.new(self.password, AES.MODE_CTR, counter=ctr)
		enc = obj.encrypt(data).hex()
		return enc


	def decrypt (self, data):
		## Exclusion du matricule.
		data = bytes.fromhex(data[8:].decode())
		ctr = new_ctr(self.iv)
		obj = AES.new(self.password, AES.MODE_CTR, counter=ctr)
		dec = obj.decrypt(data)
		return json.loads(dec.decode())


	def send_request(self, data, waitResp=True):
		self.iv = new_iv()
		sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
		encdata = self.matricule + self.iv + self.encrypt(data)
		sock.sendto(encdata.encode(), (self.server_ip, self.server_port))
		if waitResp:
			resp, server = sock.recvfrom(4096)
			resp = self.decrypt(resp)
			return resp
		return None


primitif = Primitif()
## Demande de requete sur "echo.request".
resp = primitif.send_request({"endpoint": "echo.request"})
if resp['ok']:
	## Requete sur "echo.request
	resp = primitif.send_request({
		"endpoint": "echo.request", 
		"request_id": resp['request_id']
	})
	print(resp)

